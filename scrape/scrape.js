require('dotenv').config({ path: '../.env' })
const BnbApiClient = require('@binance-chain/javascript-sdk');
const bnbClient = new BnbApiClient(process.env.CHAIN_APIURI);
const fs = require('fs');
// const ethers = require('ethers');
// const TOKEN = require('./token.json')
// const BigNumber = require('bignumber.js');
const axios = require('axios');
const { default: tx } = require('@binance-chain/javascript-sdk/lib/tx');

let data = [];

const ADDRESS = 'bnb1z7kmwvvldnq2s2lxtwhcq7h5qjekpj6dr680ue'
const RUNE_SYMBOL = 'RUNE-B1A'
const BASE_URL = 'https://dex.binance.org/api/v1/transactions'
const START_TIME_STAKE = 1563621490000
const START_TIME_NFT = 1595848277000
const TWO_WEEKS = 1310400000
const ONE_MONTH = 2620800000
const THREE_MONTHS = 7862400000

// First get all tx on NFT address
// then get an array of from addresses, create object with eth address
// then get current locked rune, add object
// then get data of first rune tx, add object
// write to file

const connect = async () => {
    console.log('CONNECTING TO BINANCE')
    await bnbClient.chooseNetwork("mainnet");
    await bnbClient.initChain()
}

const parseAddresses = async () => {
    console.log(`GETTING TRANSACTIONS FOR ${ADDRESS}`)

    let startTime = START_TIME_NFT
    let now = new Date()
    let delta = now - startTime
    let numberOfPeriods = Math.ceil(delta / TWO_WEEKS)
    console.log(numberOfPeriods)

    for (let i = 0; i < numberOfPeriods; i++) {
        let endTime = startTime + TWO_WEEKS
        // https://dex.binance.org/api/v1/transactions?address=bnb1z7kmwvvldnq2s2lxtwhcq7h5qjekpj6dr680ue&startTime=1595848277000&limit=100
        // https://dex.binance.org/api/v1/transactions?address=bnb1z7kmwvvldnq2s2lxtwhcq7h5qjekpj6dr680ue&startTime=1595848277000&endTime=1602155477000&limit=100
        console.log(`${BASE_URL}?address=${ADDRESS}&startTime=${startTime}&endTime=${endTime}&limit=1000`)

        let resp = await axios.get(`${BASE_URL}?address=${ADDRESS}&startTime=${startTime}&endTime=${endTime}&limit=1000`)
        console.log(resp.data.tx.length)
        let transactions = resp.data.tx
        let address;
        for (let i = 0; i < transactions.length; i++) {
            address = transactions[i].fromAddr
            let obj = {
                "bnbAddress": transactions[i].fromAddr,
                "ethAddress": transactions[i].memo,
            }
            let eth = transactions[i].memo
            if(eth.length != 42 || eth.substring(0,2) != '0x'){
                console.log(eth)
            }
            data.push(obj)
        }
        startTime = endTime+1000
    }

    await saveData()
}

// const getBalances = async (address) => {
//     let balances = await bnbClient.getBalance(address)
//     let frozenBalance; let freeBalance;
//     try {
//         let balance = balances.filter(x => x.symbol == RUNE_SYMBOL).map(x => x.free)
//         freeBalance = Number(balance[0]).toFixed(2);
//         console.log(`${address} free: ${freeBalance}`)
//         let frozenBalancer = balances.filter(x => x.symbol == RUNE_SYMBOL).map(x => x.frozen)
//         frozenBalance = Number(frozenBalancer[0]).toFixed(2);
//         console.log(`${address} frozen: ${frozenBalance}`)
//     } catch (e) {
//         console.log(e)
//     }
//     return {free: freeBalance, frozen:frozenBalance, total: +freeBalance + +frozenBalance}
// }

// const getTimeStaked = async (address) => {
//     let startTime = START_TIME_STAKE
//     let endTime = START_TIME_STAKE + THREE_MONTHS
//     let epoch = 1
//     let txData = []
//     let response

//     do {
//         await stall(250);
//         response = await axios.get(`${BASE_URL}?address=${address}&txAsset=${RUNE_SYMBOL}&limit=1000&startTime=${startTime}&endTime=${endTime}&txType=FREEZE_TOKEN`)
//         console.log(`${response.data.tx.length} transactions found`)
//         if (response.data.tx.length > 0){
//             txData.push(response.data.tx[0])
//         }
//         epoch ++
//         startTime = endTime
//         endTime = endTime + THREE_MONTHS
//     } while (epoch < 6 && txData.length == 0)

//     let timeStaked

//     if (txData.length == 0){
//         let now = new Date()
//         let timeStr = `${now.getFullYear()}-${now.getMonth()}-${now.getDate()}`
//         timeStaked = timeStr
//     } else {
//         timeStaked = txData[0].timeStamp
//     }
//     return timeStaked
// }

const saveData = async () => {
    console.log(`Members Registered:${data.length}`)
    fs.writeFileSync(`./registry/addressDataRaw.json`, JSON.stringify(data, null, 4), 'utf8')
    let now = new Date()
    let timeStr = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`
    return fs.writeFileSync(`./registry/addressData=${timeStr}.json`, JSON.stringify(data, null, 4), 'utf8')
}

const cleanList = async () => {
    let raw = fs.readFileSync('./registry/addressData.json', 'utf8')
    let addressData = JSON.parse(raw);
    console.log(`${addressData.length} Members to Check`)
    console.log(addressData[addressData.length-1])
    // first reverse
    let reversedData = addressData.reverse()
    console.log(reversedData[0])
    console.log(reversedData.length)
    let filteredData = []

    for(let i =0; i<reversedData.length; i++){
        let address = reversedData[i].bnbAddress
        let objFound = reversedData.find((obj) => obj.bnbAddress === address)
        let eth = objFound.ethAddress
        let len = eth.length
        if(eth.substring(0,2) === '0x' || eth.substring(len-3, len) === 'eth' || eth.substring(len-3, len) === 'xyz'){
            filteredData.push(objFound)
        }
        
    }
    data = [... new Set(filteredData)]
    console.log(data.length)

}

// const stall = async (ms = 1500) => {
//     await new Promise(resolve => setTimeout(resolve, ms));
// }

const main = async () => {
    // await connect()
    await parseAddresses();
    // await cleanList()
    await saveData()
}

main()

