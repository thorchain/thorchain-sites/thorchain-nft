// const BnbApiClient = require('@binance-chain/javascript-sdk');
// const bnbClient = new BnbApiClient(process.env.CHAIN_APIURI);
const fs = require('fs');
const axios = require('axios');

// let block = 98891245
// let week = 50
// let total = 155

// let date = '2020-10-5'
// let week = 63

let BLOCK_FIRST = 14588592
let BLOCK_IDO = 21515101

const BASE_URL = 'https://dex.binance.org/api'
let memberData = []
let tradeData = []

const parseAddresses = async () => {
    console.log(`GETTING TRANSACTIONS`)
    let resp = await axios.get(`${BASE_URL}/v2/transactions-in-block/${block}`)

    let tx = resp.data.tx

    for (let i = 0; i < tx.length; i++) {
        let subTx = tx[i].subTransactions
        if (subTx) {
            console.log(subTx.length)
            for (let j = 0; j < subTx.length; j++) {

                let memberObj = {
                    'address': subTx[j].toAddr,
                    // 'amount': subTx[j].value * 100
                    'amount': subTx[j].value * (total * 1000000) / 1000000
                }
                memberData.push(memberObj)
            }
        }
    }
    saveData()
}

const parseFromPreviousSnapshot = async () => {
    console.log(`GETTING TRANSACTIONS FROM SNAPSHOT`)

    let data = fs.readFileSync(`../rv/snapshots/RUNE_balances_${date}.json`, 'utf8')
    let snapshotData = JSON.parse(data);
    console.log(snapshotData.length)

    for (let i = 0; i < snapshotData.length; i++) {
        let memberObj = {
            'address': snapshotData[i].address,
            'amount': snapshotData[i].frozen,
        }
        memberData.push(memberObj)
    }
    saveData()
}

const parseFromIDO = async () => {
    console.log(`GETTING ADDRESSES FROM IDO`)
    let symbol = 'RUNE-B1A_BNB'
    let start1 = 1563494400000
    let end1 = 1563613304440
    // https://dex.binance.org/api/v1/trades?symbol=RUNE-B1A_BNB&start=1563494400000&end=1563667200000

    let resp = await axios.get(`${BASE_URL}/v1/trades?symbol=${symbol}&start=${start1}&end=${end1}&limit=1000`)
    let tradeCount = resp.data.trade.length
    console.log(`tradeCount ${tradeCount}`)

    for (let i = 0; i < tradeCount; i++) {
        tradeData.push(resp.data.trade[i].buyerId)
        tradeData.push(resp.data.trade[i].sellerId)
    }

    let uniqueData1 = [...new Set(tradeData)];
    console.log('uniqueData1', uniqueData1.length)

    let start2 = 1563613304440
    let end2 = 1563753600000

    resp = await axios.get(`${BASE_URL}/v1/trades?symbol=${symbol}&start=${start2}&end=${end2}&limit=1000`)

    tradeCount = resp.data.trade.length
    console.log(`tradeCount ${tradeCount}`)

    for (let i = 0; i < tradeCount; i++) {
        uniqueData1.push(resp.data.trade[i].buyerId)
        uniqueData1.push(resp.data.trade[i].sellerId)
    }

    let uniqueData = [...new Set(uniqueData1)];
    console.log('uniqueData', uniqueData.length)

    saveTradeData(uniqueData)

}

const parseFromPRESALE = async () => {
    let data = fs.readFileSync(`./transferAddresses.csv`, 'utf8')
    let snapshotData = []
    if (data.includes('\n')) {
        let rows = data.split('\n')
        for (let i = 0; i < rows.length; i++) {
            let rowData = rows[i]
            if(rowData.substring(0, 3) == "bnb"){
                console.log(rowData)
                snapshotData.push(rowData)
            }
        }
    }
    let uniqueData = [...new Set(snapshotData)];
    console.log('uniqueData', uniqueData.length)
    saveTransferData(uniqueData)
}

const parseWeek0 = async () => {
    let raw = fs.readFileSync('./registry/tradeData.json', 'utf8')
    addressData1 = JSON.parse(raw);
    let raw2 = fs.readFileSync('./registry/transferData.json', 'utf8')
    addressData2 = JSON.parse(raw2);
    let addressData = addressData1.concat(addressData2)
    console.log('addressData', addressData.length)
    // let snapshotData = []
    // for (let i = 0; i < addressData.length; i++) {
    //     snapshotData.push()
    // }

    let uniqueData = [...new Set(addressData)];
    console.log('uniqueData', uniqueData.length)
    await saveAddressData(uniqueData)
}

const saveData = () => {
    var jsonObject = JSON.stringify(memberData);
    var csv = convertToCSV(jsonObject);
    // console.log(csv)
    return fs.writeFileSync(`./snapshots/week${week}.csv`, csv, 'utf8')
}
const saveTradeData = (uniqueData) => {
    return fs.writeFileSync(`./tradeData.json`, JSON.stringify(uniqueData, null, 4), 'utf8')
}
const saveTransferData = (uniqueData) => {
    return fs.writeFileSync(`./transferData.json`, JSON.stringify(uniqueData, null, 4), 'utf8')
}
const saveAddressData = (uniqueData) => {
    return fs.writeFileSync(`./snapshots/week0.json`, JSON.stringify(uniqueData, null, 4), 'utf8')
}

function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += '\t'

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}

const getWeekData = (i) => {
    let data = fs.readFileSync(`./snapshots/weekxx.csv`, 'utf8')
    // console.log(data)
    let snapshotData = []
    if (data.includes('\r\n')) {
        let rows = data.split('\r\n')
        // console.log(rows)
        for (let i = 0; i < rows.length; i++) {
            let rowData = rows[i]
            let dataSplit = rowData.split(',')
            // console.log(dataSplit)
            if (+dataSplit[1]) {
                let freezeData = {
                    address: dataSplit[0],
                    amount: +dataSplit[1]
                }
                snapshotData.push(freezeData)
            }

        }
    }
    console.log(snapshotData)
}

const main = async () => {
    // await parseAddresses();
    // await parseFromPreviousSnapshot()
    // await parseFromIDO()
    // await parseFromPRESALE()
    await parseWeek0()
    // getWeekData()
}

main()