const fs = require('fs');

let tokenData = []

const loadData = async () => {
    let raw = fs.readFileSync('./tokenData.json', 'utf8')
    tokenData = JSON.parse(raw);
    console.log(`Tokens to Mint: ${tokenData.length}`)
}

const mintTokens = async () =>{
    for (const token of tokenData){
        await mintToken(token)
    }
}

const mintToken = async (token) => {
    console.log(`Minting token ID ${token.tokenID} name ${token.name} ${token.count} times`) 
    // mint token with EthersJS
}

const main = async () => {
    await loadData();
    mintTokens()
}

main()