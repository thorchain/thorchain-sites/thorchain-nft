const fs = require('fs');
const axios = require('axios');

let addressData = [];
let allWeekData = [];
let maxStakeData = [];
let stakelist = [];
let stakeTierData = [];

let ops1 = '0x4BeE0d0d6C437a9Ac4a24088824a8660bb18431C'
let ops2 = '0x5fb4a0983daD1A0fae31ED67F1aaffeA3F3E8eC2'
let ops3 = '0x833652029fBB7bCbd51E0f5921DF143305DbB856'
let ops4 = '0x5c6d2ac4A20A1f5812Dc7229B9812d10d798F17a'
let ops5 = '0x1D9c41971a52C6559636C349D8d8f7F14B2bBAE1'

let tokens = [3, 17, 20, 23, 32, 41, 56, 67, 70, 82]

// First get all tx on address
// then get an array of from addresses, create object with eth address
// then get current locked rune, add object
// then get data of first rune tx, add object
// write to file


const loadData = async () => {
    let raw = fs.readFileSync('./registry/addressData.json', 'utf8')
    addressData = JSON.parse(raw);
    console.log(`${addressData.length} Members to Check`)
    let raw2 = fs.readFileSync('./allocation/weekDataWithMembers.json', 'utf8')
    allWeekData = JSON.parse(raw2);
}

const filterData = () => {
    stakeList = getStakeList()
    let start = 0
    let end = addressData.length
    for (let i = start; i < end; i++) {
        let member = addressData[i].bnbAddress
        // console.log(member)
        let memberMaxStake = getMaxStake(member)
        // console.log(memberMaxStake)
        if (memberMaxStake) {
            maxStakeData.push(memberMaxStake)
        }
    }
    saveData(maxStakeData)
}

const getMaxStake = (member) => {
    let memberObj
    let maxStakeFound = 0
    for (let i = 0; i < allWeekData.length; i++) {
        let memberArray = allWeekData[i].memberArray
        let memberDetails = memberArray.find((obj) => obj.bnbAddress === member)
        // console.log(memberDetails)
        if (memberDetails && memberDetails.amount > maxStakeFound) {
            maxStakeFound = memberDetails.amount
            // console.log('new max found', maxStakeFound)
            memberObj = {
                'bnbAddress': memberDetails.bnbAddress,
                'ethAddress': memberDetails.ethAddress,
                'maxStake': maxStakeFound
            }
        }

    }
    console.log(memberObj)
    return memberObj
}

const saveData = async (data) => {
    console.log(`Entries: ${data.length}`)
    let now = new Date()
    let timeStr = `${now.getFullYear()}-${now.getMonth()}-${now.getDate()}`
    fs.writeFileSync(`./registry/master-StakeAllocation.json`, JSON.stringify(data, null, 4), 'utf8')
    fs.writeFileSync(`./registry/master-StakeAllocation-${timeStr}.json`, JSON.stringify(data, null, 4), 'utf8')
}

const getStakeList = (i) => {
    let data = fs.readFileSync(`./allocation/stakeList.csv`, 'utf8')
    let tokenData = []
    if (data.includes('\n')) {
        let rows = data.split('\n')
        for (let i = 0; i < rows.length; i++) {
            let rowData = rows[i]
            let dataSplit = rowData.split('\t')
            let item = {
                'stake': +dataSplit[0],
                'count': +dataSplit[1],
                'ID': +dataSplit[2],
            }
            tokenData.push(item)
        }
    }
    // console.log(tokenData)
    return tokenData
}

const loadStakeData = async () => {
    let raw = fs.readFileSync('./registry/master-StakeAllocation.json', 'utf8')
    maxStakeData = JSON.parse(raw);
    stakeList = getStakeList()
    console.log(`${maxStakeData.length} Members to Check`)
}

const sortIntoGroups = async () => {
    console.log(`Checking ${maxStakeData.length}`)
    let stakeGroupDetail = []
    for (let i = 0; i < stakeList.length; i++) {
        let tierMembers = []
        let tier = stakeList[i].stake
        console.log(tier)

        for (let i = 0; i < maxStakeData.length; i++) {
            let member = maxStakeData[i]
            if (member.maxStake > tier) {
                // console.log(member)
                tierMembers.push(member.ethAddress)
            }
        }
        let tierDetail = {
            'tier': tier,
            'ID': stakeList[i].ID,
            'count': stakeList[i].count,
            'total': tierMembers.length,
            'members': tierMembers
        }
        console.log(`tierDetail ${tier} members: ${tierMembers.length}`)
        stakeGroupDetail.push(tierDetail)
    }

    console.log('stakeGroupDetail.length', stakeGroupDetail.length)

    await saveStakeData(stakeGroupDetail)
}

const saveStakeData = async (data) => {
    console.log(`Entries: ${data.length}`)
    fs.writeFileSync(`./allocation/stakeTierAllocation.json`, JSON.stringify(data, null, 4), 'utf8')
}

const loadStakeTierData = async () => {
    let raw = fs.readFileSync('./allocation/stakeTierAllocation.json', 'utf8')
    stakeTierData = JSON.parse(raw);
    console.log(`${stakeTierData.length} Members to Check`)
}

const filterStakeData = async () => {
    // first shuffle the array
    // then pick the first count elements
    // save
    let stakeFilteredDetails = []

    for (let i = 0; i < stakeTierData.length; i++) {
        let members = stakeTierData[i].members
        console.log(members.length)

        let count = stakeTierData[i].count
        let tier = stakeTierData[i].tier

        let shuffledMembers = shuffle(members)
        console.log(shuffledMembers.length)
        
        let filteredMembers = shuffledMembers.slice(0, count)

        filteredMembers = addOps(filteredMembers, tier)

        let tierDetail = {
            'tier': tier,
            'tokenID': stakeTierData[i].ID,
            'members': filteredMembers.length,
            'memberArray': filteredMembers
        }
        console.log(`tierDetail ${tier} members: ${filteredMembers.length}`)
        stakeFilteredDetails.push(tierDetail)
    }
    saveStakeFilteredData(stakeFilteredDetails)

}

const addOps = (tierMembers, tier) => {
    tierMembers.push(ops1)
    if (tier != 2500000) {
        tierMembers.push(ops2)
    }
    if (tier != 3000000 && tier != 2500000) {
        tierMembers.push(ops3)
    }

    if (tier === 4000000) {
        tierMembers.push(ops4)
    }

    if (tier === 900000) {
        tierMembers.push(ops4)
        tierMembers.push(ops5)
        tierMembers.push(ops1)
    }
    if (tier === 800000) {
        tierMembers.push(ops4)
        tierMembers.push(ops5)
        tierMembers.push(ops1)
        tierMembers.push(ops2)
        tierMembers.push(ops3)
    }
    if (tier === 700000) {
        tierMembers.push(ops4)
        tierMembers.push(ops5)
        tierMembers.push(ops1)
    }
    if (tier === 600000) {
        tierMembers.push(ops4)
        tierMembers.push(ops5)
    }
    if(tier === 500000 || tier === 400000 || tier === 300000 || tier === 200000 || tier === 100000){
        tierMembers = tierMembers.slice(3,tierMembers.length)
    }
    if (tier === 90000) {
        tierMembers = tierMembers.slice(3,75)
    }
    if (tier === 60000) {
        tierMembers = tierMembers.slice(3,84)
    }
    if (tier === 30000) {
        tierMembers = tierMembers.slice(3,93)
    }
    if (tier === 10000) {
        tierMembers = tierMembers.slice(3,102)
    }

    return tierMembers
}

const saveStakeFilteredData = async (data) => {
    console.log(`Entries: ${data.length}`)
    fs.writeFileSync(`./allocation/stakeFilteredAllocation.json`, JSON.stringify(data, null, 4), 'utf8')
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

const main = async () => {
    // await loadData();
    // filterData()

    // await loadStakeData();
    // sortIntoGroups()

    await loadStakeTierData();
    filterStakeData()


    // console.log(`First checking existing holders, ${tokenData.length}`)
    // await assign(tokenData)
    // console.log(`Then checking new holders, ${addressData.length}`)
    // await assign(addressData)
}

main()

// const sortIntoGroups = async () => {
//     console.log(`Checking ${data.length}`)
//     for (let i = 0; i < data.length; i++) {
//         // get address obj
//         let addressObj = data[i]
//         console.log(`Checking ${addressObj.bnbAddress}`)
//         // see if it exists in new tokenData
//         let tokenObj = tokenData.find(obj => obj.bnbAddress === addressObj.bnbAddress)

//         // if it does, fetch tokenIDArray
//         let tokenIDArray = []
//         if(tokenObj){
//             tokenIDArray = tokenObj.tokenIDArray ? tokenObj.tokenIDArray : []
//             console.log(`Exists, has ${tokenIDArray.length} already allocated`)
//         }


//         // if existingArray = 3, then add to array from 3rd, empty txIDs
//         let existingMaxId = tokenIDArray.length
//         let newTokenIDArray = []
//         for (let i = existingMaxId; i < stakeSizeArray.length; i++){
//             if(addressObj.totalAmount > stakeSizeArray[i]*1000){
//                 console.log(`Eligible for ${stakeSizeToken[i]}`)
//                 newTokenIDArray.push({'token':stakeSizeToken[i], 'txId':""})
//             }
//         }

//         // if new tokens to allocate
//         if(newTokenIDArray.length>0){
//             // compose new obj
//             let newObj = {
//                 "bnbAddress": addressObj.bnbAddress,
//                 "ethAddress": addressObj.ethAddress,
//                 "dateStaked": addressObj.dateStaked,
//                 "stakedAmount": addressObj.stakedAmount,
//                 "unstakedAmount": addressObj.unstakedAmount,
//                 "totalAmount": addressObj.totalAmount,
//                 "tokenIDArray":tokenIDArray
//             }

//             // store
//             newTokenData[i] = newObj
//         }
//     }
//     await saveData()
// }

