const fs = require('fs');
const axios = require('axios');

let dateData = [];
let stakeData = [];


const loadData = async () => {
    let raw = fs.readFileSync('./allocation/dateAllocations-final.json', 'utf8')
    dateData = JSON.parse(raw);
    let raw2 = fs.readFileSync('./allocation/stakeAllocations-final.json', 'utf8')
    stakeData = JSON.parse(raw2);
}

const auditData = () => {
    let total = 0
    let start = 0
    let end = dateData.length
    console.log(`Date-Based Allocations`)
    for (let i = start; i < end; i++) {
        let data = dateData[i]
        // console.log(data)
        let week = data.week
        let ID = data.tokenID
        let memberCount = data.memberArray.length
        console.log(`ID: ${ID} |  Members: ${memberCount} | Week: ${week} | `)
        total = total + memberCount
    }
    console.log(`Members: ${total}`)
    end = stakeData.length
    console.log(`Stake-Based Allocations`)
    let total2 = 0
    for (let i = start; i < end; i++) {
        let data = stakeData[i]
        let tier = data.tier
        let ID = data.tokenID
        let memberCount = data.memberArray.length
        console.log(`ID: ${ID} |  Members: ${memberCount} | Tier: ${tier} | `)
        total2 = total2 + memberCount
    }
    console.log(`Members: ${total2}`)
    console.log(`Total: ${total + total2}`)
}


const main = async () => {
    await loadData();
    auditData()
}

main()
