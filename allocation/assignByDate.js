const fs = require('fs');

let addressData = [];
let weeksObject = []
let tokenList = [];

let ops1 = '0x4BeE0d0d6C437a9Ac4a24088824a8660bb18431C'
let ops2 = '0x5fb4a0983daD1A0fae31ED67F1aaffeA3F3E8eC2'

let WEEK_START = 1
let WEEK_END = 58;

let tokens = [3, 17, 20, 23, 32, 41, 56, 67, 70, 82]
let winners = ['0x8953067a30d8a05e706fdd2000fa910a29be109f', '0x7a237A323cD979B57fFd26B0bEE913F8DBF9dC31', '0x367Bc1a9d1EF225307666E330E127EAF19B2160a']
let winningTokens = [3, 14, 23]

// First get a new member
// Then for each week, check and add
// Then choose 9/18 random members. 
// Save

const loadData = async () => {
    let raw = fs.readFileSync('../registry/addressData.json', 'utf8')
    addressData = JSON.parse(raw);
    console.log(`${addressData.length} Members to Check`)
}

const check = async () => {

    let snapshotData = [];

    tokenList = getTokenList()

    for (let week = WEEK_START; week <= WEEK_END; week++) {
        // For each week, get weekData
        snapshotData = getFilteredWeekData(week)

        let tokenForWeek = tokenList.find((obj) => obj.week === week)
        console.log(tokenForWeek)

        let filteredMembers = []
        let addresses = []
        // Assign Ops
        let count = tokenForWeek.count

        for (let i = 0; i < count; i++) {
            // Choose random member from week
            // let randomMemberFromWeek = snapshotData[Math.floor(Math.random() * snapshotData.length)];
            // // console.log(randomMemberFromWeek)

            // let memberObj = addressData.find(obj => obj.bnbAddress === randomMemberFromWeek.bnbAddress)
            // // console.log(memberObj)
            // foundAddresses.push(randomMemberFromWeek.ethAddress)
            // let addresses = snapshotData.map((obj) => obj.ethAddress)
            let shuffledMembers = shuffle(snapshotData)
            console.log(shuffledMembers.length)
            filteredMembers = shuffledMembers.slice(0, count-1)
            console.log(filteredMembers.length)
            filteredMembers = addOps(filteredMembers, tokenForWeek.ID)
            filteredMembers = addWinners(filteredMembers, tokenForWeek.ID)
            addresses = filteredMembers.map((obj) => obj.ethAddress)
        }
        let totalMembers = filteredMembers.length
        let weekData = {
            "week": week,
            "tokenID": tokenForWeek.ID,
            'members': totalMembers,
            'memberArray': addresses
        }
        console.log(`Week: ${week} | Members: ${totalMembers}`)
        weeksObject.push(weekData)
    }

    // console.log(`${weeksObject.length}`)
    // console.log(weeksObject[0].memberArray[0])

}

const addOps = (tierMembers, ID) => {
    let member = {
        'ethAddress': ops2
    }
    tierMembers.push(member)
    if (tokens.find((obj) => obj === ID)) {
        tierMembers = tierMembers.slice(1,tierMembers.count)
        let member = {
            'ethAddress': ops1
        }
        tierMembers.push(member)
    }
    return tierMembers
}

const addWinners = (tierMembers, ID) => {
    if (winningTokens.find((obj) => obj === ID)) {
        let index = winningTokens.indexOf(ID)
        let member = {
            'ethAddress': winners[index]
        }
        tierMembers.push(member)
    }
    return tierMembers
}

const checkWeek0 = (data) => {
    let raw = fs.readFileSync('./snapshots/week0.json', 'utf8')
    let week0Data = JSON.parse(raw);
    console.log(`Genesis participants ${week0Data.length}`)
    let week0Allocation = []
    let week0List = []
    for (let i = 0; i < data.length; i++) {
        // For each member, check if present in week0
        // get address obj
        let addressObj = data[i]
        // console.log(`Checking ${addressObj.bnbAddress}`)
        let tokenObj = week0Data.find(obj => obj === addressObj.bnbAddress)
        // console.log(tokenObj)

        if (tokenObj) {
            let memberObj = {
                bnbAddress: addressObj.bnbAddress,
                ethAddress: addressObj.ethAddress,
                tokenID: 0
            }
            week0Allocation.push(memberObj)
            week0List.push(addressObj.ethAddress)
        }
    }
    console.log(`Genesis members ${week0Allocation.length}`)
    console.log(`Writing them all to Genesis List, ${week0Allocation.length}`)
    fs.writeFileSync(`./allocation/week0Members.json`, JSON.stringify(week0Allocation, null, 4), 'utf8')
    fs.writeFileSync(`./allocation/week0List.json`, JSON.stringify(week0List, null, 4), 'utf8')
}

const getWeekData = (i) => {
    let data = fs.readFileSync(`./snapshots/week${i}.csv`, 'utf8')
    let snapshotData = []
    if (data.includes('\n')) {
        let rows = data.split('\n')
        for (let i = 0; i < rows.length; i++) {
            let rowData = rows[i]
            let dataSplit = rowData.split('\t')
            let freezeData = {
                address: dataSplit[0],
                amount: +dataSplit[1]
            }
            snapshotData.push(freezeData)
        }
    }
    return snapshotData
}

const getTokenList = (i) => {
    let data = fs.readFileSync(`./tokenList.csv`, 'utf8')
    let tokenData = []
    if (data.includes('\n')) {
        let rows = data.split('\n')
        for (let i = 0; i < rows.length; i++) {
            let rowData = rows[i]
            let dataSplit = rowData.split('\t')
            let item = {
                'week': +dataSplit[0],
                'ID': +dataSplit[1],
                'count': +dataSplit[2],

            }
            tokenData.push(item)
        }
    }
    // console.log(tokenData)
    return tokenData
}

const getFilteredWeekData = (week) => {
    let raw = fs.readFileSync('./cleanedWeekData.json', 'utf8')
    let allWeekData = JSON.parse(raw);
    let weekData = allWeekData.find((obj) => obj.week === week)
    console.log(`Members Elegible: ${weekData.memberArray.length}`)
    return weekData.memberArray
}

const saveData = (week) => {
    let ID_START = tokenList.find((obj) => obj.week === WEEK_START)
    let ID_END = tokenList.find((obj) => obj.week === WEEK_END)
    console.log(`Writing them all to new MasterList, ${weeksObject.length}`)
    return fs.writeFileSync(`./tokenList${ID_START.ID}-${ID_END.ID}.json`, JSON.stringify(weeksObject, null, 4), 'utf8')
}

const loadWeekData = () => {
    let raw = fs.readFileSync('./weekDataWithMembers.json', 'utf8')
    let allWeekData = JSON.parse(raw);
    return allWeekData
}

const cleanWeekData = () => {
    let weekData = loadWeekData()
    let newWeekData = []
    for(let i = 0; i<weekData.length; i++){
        let weekDetails = weekData[i]
        // console.log(weekDetails)
        let newMemberArray = []
        for(let j = 0; j<weekDetails.memberArray.length; j++){

            let eth = weekDetails.memberArray[j].ethAddress
            let len = eth.length
            if((eth.substring(0,2) === '0x' && len === 42) || eth.substring(len-3, len) === 'eth' || eth.substring(len-3, len) === 'xyz'){
                let newDeets = {
                    'bnbAddress': weekDetails.memberArray[j].bnbAddress,
                    'ethAddress': weekDetails.memberArray[j].ethAddress,
                    'amount': weekDetails.memberArray[j].amount
                }
                newMemberArray.push(newDeets)
            }
        }
        let newWeekDetails =  {
            week : weekDetails.week,
            members : newMemberArray.length,
            memberArray: newMemberArray
        }
        newWeekData.push(newWeekDetails)
    }
    saveWeekData(newWeekData)
}

const saveWeekData = (data) => {
    return fs.writeFileSync(`./cleanedWeekData.json`, JSON.stringify(data, null, 4), 'utf8')
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

const main = async () => {
    
    // // console.log(`First checking existing holders, ${tokenData.length}`)
    // // await assign(tokenData)
    // console.log(`Checking holders, ${addressData.length}`)
    // // await checkWeek0(addressData)

    await loadData();
    await check(addressData)
    saveData()

    // await loadWeekData()
    // cleanWeekData()
}

main()

